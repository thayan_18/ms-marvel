package br.com.marvel.model;

import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharacterDataContainer extends DataContainer {
	@JsonbProperty("results")
	private List<Characters> listCharacters;
}
