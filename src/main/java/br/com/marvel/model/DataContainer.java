package br.com.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataContainer {
	private Integer offset;
	private Integer limit;
	private Integer total;
	private Integer count;
}
