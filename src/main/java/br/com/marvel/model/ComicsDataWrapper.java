package br.com.marvel.model;

import javax.json.bind.annotation.JsonbProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ComicsDataWrapper extends DataWrapper {
	@JsonbProperty("data")
	private ComicsDataContainer dataComics;
}
