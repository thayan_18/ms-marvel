package br.com.marvel.model;

import javax.json.bind.annotation.JsonbProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharacterDataWrapper extends DataWrapper {
	@JsonbProperty("data")
	private CharacterDataContainer dataCharacter;
}
