package br.com.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comics {
	private Integer id;
	private Integer digitalId;
	private String title;
	private Double issueNumber;
	private String variantDescription;
	private String description;
}
