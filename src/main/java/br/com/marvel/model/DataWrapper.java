package br.com.marvel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataWrapper {
	private Integer code;
	private String status;
	private String copyright;
	private String etag;
}
