package br.com.marvel.client;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import br.com.marvel.model.CharacterDataWrapper;
import br.com.marvel.model.ComicsDataWrapper;
import br.com.marvel.service.ClientRequestService;

@ApplicationScoped
@RegisterRestClient(configKey = "config.api.marvel")
@RegisterProvider(ClientRequestService.class)
public interface MarvelClient {

	@GET
	@Path("/characters")
	CharacterDataWrapper getAllCharacters(@QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit);

	@GET
	@Path("/characters/{characterId}/comics")
	ComicsDataWrapper getAllComicsByCharacterId(@PathParam("characterId") Long characterId,
			@QueryParam("offset") Integer offset,
			@QueryParam("limit") Integer limit);
}
