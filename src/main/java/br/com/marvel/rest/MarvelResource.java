package br.com.marvel.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import br.com.marvel.common.Constantes;
import br.com.marvel.exceptions.ErroResponse;
import br.com.marvel.model.CharacterDataWrapper;
import br.com.marvel.model.ComicsDataWrapper;
import br.com.marvel.service.MarvelService;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/public")
public class MarvelResource {

	private static final String SC_CONFLICT = "409";

	@Inject
	MarvelService marvelService;

	@GET
	@Path("/getAllCharacters")
	@Operation(summary = "Fetches lists of characters.")
	@APIResponse(responseCode = SC_CONFLICT, content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = ErroResponse.class)))
	public CharacterDataWrapper getAllCharacters(
			@DefaultValue(Constantes.QUERY_PARAM_OFFSET_DEFAULT_VALUE) @QueryParam(Constantes.QUERY_PARAM_OFFSET) Integer offset,
			@DefaultValue(Constantes.QUERY_PARAM_LIMIT_DEFAULT_VALUE) @QueryParam(Constantes.QUERY_PARAM_LIMIT) Integer limit) {
		return marvelService.getAllCharacters(offset, limit);
	}

	@GET
	@Path("/getAllComicsByCharacterId/{characterId}")
	@Operation(summary = "Fetches lists of comics filtered by a character id.")
	public ComicsDataWrapper getAllComicsByCharacterId(
			@PathParam("characterId") Long characterId,
			@DefaultValue(Constantes.QUERY_PARAM_OFFSET_DEFAULT_VALUE) @QueryParam(Constantes.QUERY_PARAM_OFFSET) Integer offset,
			@DefaultValue(Constantes.QUERY_PARAM_LIMIT_DEFAULT_VALUE) @QueryParam(Constantes.QUERY_PARAM_LIMIT) Integer limit) {
		return marvelService.getAllComicsByCharacterId(characterId, offset, limit);
	}
}
