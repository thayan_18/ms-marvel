package br.com.marvel.common;

public final class Constantes {

	private Constantes() {
	}

	public static final String QUERY_PARAM_OFFSET = "offset";
	public static final String QUERY_PARAM_LIMIT = "limit";
	public static final String QUERY_PARAM_OFFSET_DEFAULT_VALUE = "0";
	public static final String QUERY_PARAM_LIMIT_DEFAULT_VALUE = "20";
}
