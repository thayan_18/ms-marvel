package br.com.marvel.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MarvelApiExceptionHandler implements ExceptionMapper<MarvelApiException> {
	@Override
	public Response toResponse(MarvelApiException exception) {
		return Response.status(Response.Status.CONFLICT).entity(exception.getErroResponse()).build();
	}
}
