package br.com.marvel.exceptions;

public class MarvelApiException extends RuntimeException {
	private ErroResponse errorResponse;

	public MarvelApiException() {
		super();
	}

	public MarvelApiException(ErroResponse errorResponse) {
		super(errorResponse.getMessage());
		this.errorResponse = errorResponse;
	}

	public ErroResponse getErroResponse() {
		return errorResponse;
	}
}
