package br.com.marvel;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class MarvelApplication {
	public static void main(String... args) {
		Quarkus.run(args);
	}
}
