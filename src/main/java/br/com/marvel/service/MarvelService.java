package br.com.marvel.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;
import org.jboss.resteasy.client.exception.ResteasyWebApplicationException;

import br.com.marvel.client.MarvelClient;
import br.com.marvel.exceptions.ErroResponse;
import br.com.marvel.exceptions.MarvelApiException;
import br.com.marvel.model.CharacterDataWrapper;
import br.com.marvel.model.ComicsDataWrapper;

@ApplicationScoped
public class MarvelService {

	private static final Logger LOGGER = Logger.getLogger(MarvelService.class);

	@Inject
	@RestClient
	MarvelClient marvelClient;

	public CharacterDataWrapper getAllCharacters(Integer offset, Integer limit) {
		LOGGER.info(String.format("getAllCharacters - start"));
		try {

			CharacterDataWrapper characterDataWrapper = marvelClient.getAllCharacters(offset, limit);
			LOGGER.info(String.format("getAllCharacters - end status code: [%s]", characterDataWrapper.getStatus()));
			return characterDataWrapper;
		} catch (ResteasyWebApplicationException exWeb) {

			LOGGER.error(String.format("getAllCharacters - error: [%s]", exWeb.getResponse().getStatus()));
			ErroResponse erroResponse = exWeb.unwrap().getResponse().readEntity(ErroResponse.class);
			throw new MarvelApiException(erroResponse);
		} catch (Exception ex) {

			LOGGER.error(String.format("getAllCharacters - error: [%s]", ex.getMessage()));
			throw new MarvelApiException(ErroResponse.builder().message(ex.getMessage()).build());
		}
	}

	public ComicsDataWrapper getAllComicsByCharacterId(Long characterId, Integer offset, Integer limit) {
		try {

			ComicsDataWrapper comicsDataWrapper = marvelClient.getAllComicsByCharacterId(characterId, offset, limit);
			LOGGER.info(String.format("getAllComicsByCharacterId - end status code: [%s]", comicsDataWrapper.getStatus()));
			return comicsDataWrapper;
		} catch (ResteasyWebApplicationException exWeb) {

			LOGGER.error(String.format("getAllComicsByCharacterId - error: [%s]", exWeb.getResponse().getStatus()));
			ErroResponse erroResponse = exWeb.unwrap().getResponse().readEntity(ErroResponse.class);
			throw new MarvelApiException(erroResponse);
		} catch (Exception ex) {

			LOGGER.error(String.format("getAllComicsByCharacterId - error: [%s]", ex.getMessage()));
			throw new MarvelApiException(ErroResponse.builder().message(ex.getMessage()).build());
		}
	}
}
