package br.com.marvel.service;

import java.io.IOException;
import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

@RequestScoped
public class ClientRequestService implements ClientRequestFilter {

	private static final Logger LOGGER = Logger.getLogger(ClientRequestService.class);
	private static final String QUERY_PARAM_API_KEY = "apikey";
	private static final String QUERY_PARAM_TS = "ts";
	private static final String QUERY_PARAM_HASH = "hash";

	@ConfigProperty(name = "config.api.marvel.private.key")
	String marvelPrivateKey;

	@ConfigProperty(name = "config.api.marvel.public.key")
	String marvelPublicKey;

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		Long timestamp = new Date().getTime();
		requestContext.setUri(UriBuilder.fromUri(requestContext.getUri()).queryParam(QUERY_PARAM_TS, timestamp)
				.queryParam(QUERY_PARAM_API_KEY, marvelPublicKey).queryParam(QUERY_PARAM_HASH, generateHashMD5(timestamp))
				.build());
		LOGGER.info(String.format("filter - url: [%s]", requestContext.getUri().toString()));
	}

	private String generateHashMD5(Long timestamp) {
		StringBuilder hash = new StringBuilder();
		hash.append(timestamp);
		hash.append(marvelPrivateKey);
		hash.append(marvelPublicKey);
		return DigestUtils.md5Hex(hash.toString());
	}
}
