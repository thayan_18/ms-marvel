package br.com.marvel.rest;

import static io.restassured.RestAssured.given;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import br.com.marvel.client.MarvelClient;
import br.com.marvel.common.MarvelResourceMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;

@QuarkusTest
public class MarvelResourceTest {

	@InjectMock
	MarvelClient marvelService;

	@Test
	void getAllCharactersTest() {
		when(marvelService.getAllCharacters(any(), any())).thenReturn(MarvelResourceMock.getAllCharactersMock());
		given()
				.contentType(ContentType.JSON)
				.when()
				.get("/public/getAllCharacters")
				.then().statusCode(HttpStatus.SC_OK);
	}

	@Test
	void getAllComicsByCharacterIdTest() {
		when(marvelService.getAllComicsByCharacterId(any(), any(), any()))
				.thenReturn(MarvelResourceMock.getAllComicsByCharacterId());
		given()
				.contentType(ContentType.JSON)
				.when()
				.get("/public/getAllComicsByCharacterId/123")
				.then().statusCode(HttpStatus.SC_OK);
	}
}
