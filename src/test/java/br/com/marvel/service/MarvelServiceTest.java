package br.com.marvel.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.Test;

import br.com.marvel.client.MarvelClient;
import br.com.marvel.common.MarvelServiceMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
public class MarvelServiceTest {

	static final Integer QUERY_PARAM_OFFSET = 0;
	static final Integer QUERY_PARAM_LIMIT = 20;
	static final Long QUERY_PARAM_CHARACTER_ID = 12345678910L;

	@InjectMock
	@RestClient
	MarvelClient marvelClient;

	@Inject
	MarvelService marvelService;

	@Test
	void getAllCharactersTest() {
		when(marvelClient.getAllCharacters(anyInt(), anyInt())).thenReturn(MarvelServiceMock.getAllCharactersMock());
		marvelService.getAllCharacters(QUERY_PARAM_OFFSET, QUERY_PARAM_LIMIT);

		verify(marvelClient, atLeastOnce()).getAllCharacters(QUERY_PARAM_OFFSET, QUERY_PARAM_LIMIT);
	}

	@Test
	void getAllComicsByCharacterId() {
		when(marvelClient.getAllComicsByCharacterId(any(), any(), any()))
				.thenReturn(MarvelServiceMock.getAllComicsByCharacterId());

		marvelService.getAllComicsByCharacterId(QUERY_PARAM_CHARACTER_ID, QUERY_PARAM_OFFSET, QUERY_PARAM_LIMIT);
		verify(marvelClient, atLeastOnce()).getAllComicsByCharacterId(QUERY_PARAM_CHARACTER_ID, QUERY_PARAM_OFFSET,
				QUERY_PARAM_LIMIT);
	}
}
