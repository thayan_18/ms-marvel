package br.com.marvel.common;

import br.com.marvel.model.CharacterDataWrapper;
import br.com.marvel.model.ComicsDataWrapper;

public final class MarvelServiceMock {
	public static CharacterDataWrapper getAllCharactersMock() {
		CharacterDataWrapper characterDataWrapper = new CharacterDataWrapper();
		return characterDataWrapper;
	}

	public static ComicsDataWrapper getAllComicsByCharacterId() {
		ComicsDataWrapper comicsDataWrapper = new ComicsDataWrapper();
		return comicsDataWrapper;
	}
}
