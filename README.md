# ms-marvel Project

Este projeto usa Quarkus, o Supersonic Subatomic Java Framework.
Se você quiser saber mais sobre o Quarkus, visite o site: https://quarkus.io/.

## Executando o aplicativo no modo dev

Para executar esse micro-service no modo dev basta executar o comando abaixo:
```shell script
./mvnw quarkus:dev
```

**_NOTE:_**
Swagger esta disponível no link http://localhost:8080/api-docs/
**_NOTE:_**
Para executar essa api Swagger, adicione sua chave da Marvel no arquivo application.properties linha 11 e 12;
**_NOTE:_**
Apache Maven 3.6.3
Java version: 11.0.10
